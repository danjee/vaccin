import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.sound.sampled.LineUnavailableException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class Vaccin {

    final static String API_URL =
            "https://programare.vaccinare-covid.gov.ro/scheduling/api/centres?page=0&size=10&sort=,";
    final static String CNP = "";
    final static String RECIPIENT = "";
    static String COOKIE = "";

    //pfizer si moderna
    final static List<Integer> TYPES = List.of(1, 2);

    final static ObjectMapper MAPPER = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    public static void main(String[] args) throws LineUnavailableException, InterruptedException {
        final List<County> locations = List.of(
                County.builder()
                        .id(40)
                        .name("Bucuresti")
                        .localities(List.of(
                                Locality.builder()
                                        .id("13119")
                                        .name("Sector 1")
                                        .build(),
                                Locality.builder()
                                        .id("13546")
                                        .name("Sector 2")
                                        .build(),
                                Locality.builder()
                                        .id("13547")
                                        .name("Sector 3")
                                        .build(),
                                Locality.builder()
                                        .id("13548")
                                        .name("Sector 4")
                                        .build(),
                                Locality.builder()
                                        .id("13549")
                                        .name("Sector 5")
                                        .build(),
                                Locality.builder()
                                        .id("13554")
                                        .name("Sector 6")
                                        .build()
                        ))
                        .build()
                ,
                County.builder()
                        .id(15)
                        .name("Dimbovita")
                        .localities(List.of(
                                Locality.builder()
                                        .id("4928")
                                        .name("Aninoasa")
                                        .build(),
                                Locality.builder()
                                        .id("5254")
                                        .name("Tirgoviste")
                                        .build()
                        ))
                        .build()

        );

        final Runnable runnable = () -> locations.forEach(county -> {
            System.out.println("searching county: " + county.getName());
            county.getLocalities().forEach(locality -> {
                System.out.println("searching locality: " + locality.getName());
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                final Request request = Request.builder()
                        .county(county)
                        .locality(locality)
                        .identificationCode(CNP)
                        .masterPersonnelCategoryID(-4)
                        .personnelCategoryID(32)
                        .recipientID(RECIPIENT)
                        .build();
                try {
                    doRequest(request);
                } catch (IOException | InterruptedException | LineUnavailableException e) {
                    e.printStackTrace();
                }
            });
        });

        ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
        exec.scheduleAtFixedRate(runnable, 0, 1, TimeUnit.MINUTES);
    }

    private static void doRequest(final Request request)
            throws IOException, LineUnavailableException, InterruptedException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(API_URL);
        final String json = createJson(request);
        StringEntity entity = new StringEntity(json);
        httpPost.setEntity(entity);
        httpPost.setHeader("accept-language", "en-US,en;q=0.9");
        httpPost.setHeader("Accept", "application/json, text/plain, */*");
        httpPost.setHeader("Content-type", "application/json");
        httpPost.setHeader("sec-ch-ua-mobile", "?0");
        httpPost.setHeader("sec-fetch-dest", "empty");
        httpPost.setHeader("sec-fetch-mode", "cors");
        httpPost.setHeader("sec-fetch-site", "same-origin");
        httpPost.setHeader("Referer", "https://programare.vaccinare-covid.gov.ro/");
        httpPost.setHeader("Referrer-Policy", "strict-origin-when-cross-origin");
        httpPost.setHeader("sec-ch-ua",
                "\"Google Chrome\";v=\"87\", \" Not;A Brand\";v=\"99\", \"Chromium\";v=\"87\"");
        httpPost.setHeader("cookie", COOKIE);

        CloseableHttpResponse response = client.execute(httpPost);
        if (response.getStatusLine().getStatusCode() == 200) {
            final Response res = MAPPER.readValue(response.getEntity().getContent(), Response.class);
            res.getContent().stream().filter(item -> {
                //Pfizer sau Moderna
                return TYPES.contains(item.getBoosterID());
            })
                    .peek(item -> {
                        if (item.getAvailableSlots() < 2) {
                            System.out.println("no results in "
                                    + request.getCounty().getName()
                                    + ", "
                                    + request.getLocality().getName()
                                    + ", "
                                    + item.getAddress()
                                    + "..."
                            );
                        }
                    })
                    .filter(item -> item.getAvailableSlots() >= 2)
                    .forEach(item -> {
                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        System.out.println(request.getCounty().getName());
                        System.out.println(request.getLocality().getName());
                        System.out.println(item.getAddress());
                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        try {
                            SoundUtils.alarmResults();
                        } catch (LineUnavailableException e) {
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    });
            ;
        } else {
            System.err.print(response.getStatusLine().getStatusCode());
            System.err.println(" --- ");
            System.err.print(response.getStatusLine().getReasonPhrase());
            SoundUtils.alarmError();
        }

        client.close();
        response.close();
    }

    private static String createJson(final Request request) throws JsonProcessingException {
        RequestSerializable serializable = RequestSerializable.builder()
                .countyID(request.getCounty().getId())
                .localityID(request.getLocality().getId())
                .masterPersonnelCategoryID(request.getMasterPersonnelCategoryID())
                .identificationCode(request.getIdentificationCode())
                .personnelCategoryID(request.getPersonnelCategoryID())
                .recipientID(request.getRecipientID())
                .build();
        return MAPPER.writeValueAsString(serializable);
    }

    private static void login() {

    }

    @Data
    @AllArgsConstructor
    @Builder
    public static class Request {
        private County county;
        private Locality locality;
        private String identificationCode;
        private Integer masterPersonnelCategoryID;
        private Integer personnelCategoryID;
        private String recipientID;
    }

    @Data
    @AllArgsConstructor
    @Builder
    public static class RequestSerializable {
        private Integer countyID;
        private String localityID;
        private String identificationCode;
        private Integer masterPersonnelCategoryID;
        private Integer personnelCategoryID;
        private String recipientID;
    }

    @Data
    public static class Response {
        private List<Content> content;
    }

    @Data
    public static class Content {
        private String address;
        private Integer availableSlots;
        private Integer boosterDays;
        private Integer boosterID;
    }

    @Data
    @AllArgsConstructor
    @Builder
    static class County {
        private Integer id;
        private String name;
        private List<Locality> localities;
    }

    @Data
    @AllArgsConstructor
    @Builder
    static class Locality {

        private String id;
        private String name;
    }
}
